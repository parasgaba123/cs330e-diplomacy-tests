from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):


    def test_read_1(self):
        s = "A Madrid Hold\n"
        i, j, k = diplomacy_read(s)
        self.assertEqual(i,  "A")
        self.assertEqual(j,  "Madrid")
        self.assertEqual(k,  "Hold")

    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i,  "B")
        self.assertEqual(j,  "Barcelona")
        self.assertEqual(k,  "Move")
        self.assertEqual(l,  "Madrid")

    def test_read_3(self):
        s = "D Paris Support B\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i,  "D")
        self.assertEqual(j,  "Paris")
        self.assertEqual(k,  "Support")
        self.assertEqual(l,  "B")
    # -----
    # eval
    # -----



    # -----
    # print
    # -----

    # def test_print_1(self):
    #     w = StringIO()
    #     diplomacy_print(w, 1, 10, 20)
    #     self.assertEqual(w.getvalue(), "1 10 20\n")


    def test_solve_1(self):
        r = StringIO("A Paris Move London\nB Barcelona Move Madrid\nC Austin Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB Madrid\nC Austin\n")

    def test_solve_2(self):
        r = StringIO("A Paris Move London\nB London Move Paris\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB Paris\n")

    def test_solve_3(self):
        r = StringIO("A London Hold\nB Paris Hold\nC Rome Support A\nD Madrid Move Paris\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB [dead]\nC Rome\nD [dead]\n")
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\nF NewYork Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE [dead]\nF [dead]\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
"""
